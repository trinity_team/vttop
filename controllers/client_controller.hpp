#ifndef CLIENTCONTROLLER_H
#define CLIENTCONTROLLER_H

#include "application_controller.hpp"

class T_CONTROLLER_EXPORT ClientController : public ApplicationController {

  Q_OBJECT

public:
  ClientController() : ApplicationController() {}

public slots:
  void index();
  void show(const QString &id);
  void create();
  void save(const QString &id);
  void remove(const QString &id);
};

#endif // CLIENTCONTROLLER_H
