#include "client.h"
#include "client_controller.hpp"

void ClientController::index()
{
    auto clientList = Client::getAll();
    texport(clientList);
    render();
}

void ClientController::show(const QString &id)
{
    auto client = Client::get(id.toInt());
    texport(client);
    render();
}

void ClientController::create()
{
    switch (httpRequest().method()) {
    case Tf::Get:
        render();
        break;

    case Tf::Post: {
        auto client = httpRequest().formItems("client");
        auto model = Client::create(client);

        if (!model.isNull()) {
            QString notice = "Created successfully.";
            tflash(notice);
            redirect(urla("show", model.id()));
        } else {
            QString error = "Failed to create.";
            texport(error);
            texport(client);
            render();
        }
        break; }

    default:
        renderErrorResponse(Tf::NotFound);
        break;
    }
}

void ClientController::save(const QString &id)
{
    switch (httpRequest().method()) {
    case Tf::Get: {
        auto model = Client::get(id.toInt());
        if (!model.isNull()) {
            auto client = model.toVariantMap();
            texport(client);
            render();
        }
        break; }

    case Tf::Post: {
        QString error;
        auto model = Client::get(id.toInt());
        
        if (model.isNull()) {
            error = "Original data not found. It may have been updated/removed by another transaction.";
            tflash(error);
            redirect(urla("save", id));
            break;
        }

        auto client = httpRequest().formItems("client");
        model.setProperties(client);
        if (model.save()) {
            QString notice = "Updated successfully.";
            tflash(notice);
            redirect(urla("show", model.id()));
        } else {
            error = "Failed to update.";
            texport(error);
            texport(client);
            render();
        }
        break; }

    default:
        renderErrorResponse(Tf::NotFound);
        break;
    }
}

void ClientController::remove(const QString &id)
{
    if (httpRequest().method() != Tf::Post) {
        renderErrorResponse(Tf::NotFound);
        return;
    }

    auto client = Client::get(id.toInt());
    client.remove();
    redirect(urla("index"));
}


// Don't remove below this line
T_DEFINE_CONTROLLER(ClientController)
