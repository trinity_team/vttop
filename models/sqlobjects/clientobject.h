#ifndef CLIENTOBJECT_H
#define CLIENTOBJECT_H

#include <TSqlObject>
#include <QSharedData>


class T_MODEL_EXPORT ClientObject : public TSqlObject, public QSharedData
{
public:
    int id {0};
    QString name;

    enum PropertyIndex {
        Id = 0,
        Name,
    };

    int primaryKeyIndex() const override { return Id; }
    int autoValueIndex() const override { return -1; }
    QString tableName() const override { return QStringLiteral("client"); }

private:    /*** Don't modify below this line ***/
    Q_OBJECT
    Q_PROPERTY(int id READ getid WRITE setid)
    T_DEFINE_PROPERTY(int, id)
    Q_PROPERTY(QString name READ getname WRITE setname)
    T_DEFINE_PROPERTY(QString, name)
};

#endif // CLIENTOBJECT_H
