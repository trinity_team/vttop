#ifndef CLIENT_H
#define CLIENT_H

#include <QStringList>
#include <QDateTime>
#include <QVariant>
#include <QSharedDataPointer>
#include <TGlobal>
#include <TAbstractModel>

class TModelObject;
class ClientObject;
class QJsonArray;


class T_MODEL_EXPORT Client : public TAbstractModel
{
public:
    Client();
    Client(const Client &other);
    Client(const ClientObject &object);
    ~Client();

    int id() const;
    void setId(int id);
    QString name() const;
    void setName(const QString &name);
    Client &operator=(const Client &other);

    bool create() override { return TAbstractModel::create(); }
    bool update() override { return TAbstractModel::update(); }
    bool save()   override { return TAbstractModel::save(); }
    bool remove() override { return TAbstractModel::remove(); }

    static Client create(int id, const QString &name);
    static Client create(const QVariantMap &values);
    static Client get(int id);
    static int count();
    static QList<Client> getAll();
    static QJsonArray getAllJson();

private:
    QSharedDataPointer<ClientObject> d;

    TModelObject *modelData() override;
    const TModelObject *modelData() const override;
    friend QDataStream &operator<<(QDataStream &ds, const Client &model);
    friend QDataStream &operator>>(QDataStream &ds, Client &model);
};

Q_DECLARE_METATYPE(Client)
Q_DECLARE_METATYPE(QList<Client>)

#endif // CLIENT_H
