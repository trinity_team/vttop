# Virtual Table Top


## Requirements
- treefrog v1.25 [github](https://github.com/treefrogframework/treefrog-framework)
- Node JS v10.16 [official site](https://nodejs.org)

## Installation
```
git clone https://gitlab.com/Harrm/vttop
cd vttop
mkdir build
cmake -B build
cmake --build build
treefrog 
# OR
treefrog -e dev # for development
```
