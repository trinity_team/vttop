# CONTRIBUTING
1. VTTop uses C++17with CMake
2. Use `clang-format`/JS formatter 
3. Test your code with GTest/Mocha
4. Open PR with `master` as the base branch, fix CI and follow guide in PR template
